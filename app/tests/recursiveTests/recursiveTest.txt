//Grabbing and storying the files into a JSON object
const fs = require('fs');
const path = require("path");
let directories = [];
let files = [];
let dir = [];
var id = 0;
var superId = -1;
var p = [];

var dataset = {
    nodes: [
    ],
    edges: [
    ]
};

function getDirectories(source){
  fs.readdirSync(source).forEach(file => {
    if (fs.statSync(source + "/" + file).isDirectory()) {
      directories.push(source + "/" + file);
  }
});
}

function getFiles(fileSource){
  getDirectories(fileSource);

  let files = fs.readdirSync(fileSource);
  let dir = directories;

  try {
    files = fs.readdirSync(p)
  } catch (err) {
    console.log('TCL: err', err)
  }

  if(dir.length === 0) {
    return files;
  }

  for(var i=0, max=dir.length; i < max; i++) {
    for (let d of dir) {
      getFiles(d);
    }
  }
}

getFiles('/');

console.log(directories);
console.log(files);
