//Grabbing and storying the files into a JSON object
const fs = require('fs');
const util = require('util');
const path = require("path");
const elasticlunr = require('elasticlunr');

let directories = [];
let files = [];
var superId = 0;
var id = 0;

var dataset = {
    nodes: [
    ],
    links: [
    ]
};

var index = {}

function getFiles(fileSource){  // define the return structure
    superId += 1; // define super ID
    dataset.nodes.push({ //push super
      size:50,
      score:0,
      id: path.basename(fileSource),
      type: "circle",
    })

    fs.readdirSync(fileSource).filter(file => file[0] !== ".").forEach(file => {
        id+=1// for every file in this directory
        stats = fs.statSync(fileSource + "/" + file); //grab the states of the current file

        files.push(file); //save files in a seperate array.

        var fileINDEX = {
          "id": id,
          "title": file,
          "path": fileSource + "/" + file
        }

        index.push(fileINDEX);

        var fileSize = stats["size"]/100; //find the size of file

        if(fileSize >= 100) { //normalize sizes
          fileSize= 100;
        }

        else if (fileSize <= 3) {
          fileSize += 2;
        }

        let fullpath = fileSource + "/" + file; // the full path to this file
        try { // wrap in a try block in case statSync throws
            if (fs.statSync(fullpath).isDirectory()) { // if it's a directory
                dataset.nodes.push(getFiles(fullpath)); // get everything in the directory and add that as a child
            } else { // it's a file
                dataset.nodes.push({ //push file as a node
                  size:fileSize,
                  score:0,
                  id: file,
                  type: "circle",
                })

                dataset.links.push({ //push file on to super
                  source: superId,
                  target: id,
                })
            }
        } catch (e) {
            console.error(e);
        };
    });

    return dataset;
  };

directories = getFiles('/Users/enepoon/Desktop');

const getCircularReplacer = () => {
  const seen = new WeakSet();
  return (key, value) => {
    if (typeof value === "object" && value !== null) {
      if (seen.has(value)) {
        return;
      }
      seen.add(value);
    }
    return value;
  };
};

var filtered = arr.filter(function(x) {
   return x !== undefined;
});

fs.writeFile("testobject.json", JSON.stringify(directories, getCircularReplacer()), (err) => {
    if (err) {   //send data to json file for graphing
        console.error(err);
        return;
      };

  });

fs.writeFile('example_index.json', JSON.stringify(index), function (err) {
  if (err) throw err;
  console.log('done');
});


// console.log(util.inspect(directories, false, null, true));
