$(document).ready(function(){
  //initialization
  var width = document.documentElement.clientWidth;
  var height = document.documentElement.clientHeight;

  //Grabbing and storying the files into a JSON object
  const fs = require('fs');
  const path = require("path");
  let directories = [];
  let files = [];
  var id = 0;
  var superId = -1;
  var source;

  var dataset = {
      nodes: [
      ],
      edges: [
      ]
  };

  function gatherData(source) {
    superId += 1;
    var source = source;

    dataset.nodes.push({
      name: path.basename(source),
      type: "super",
      isAFile: "no",
      size: 20,
      path: source,
    })

    fs.readdirSync(source).forEach(file => {
      id += 1;
      stats = fs.statSync(source + "/" + file);

      //check if the file is bigger than 10, if so
      var fileSize = stats["size"]/100;

      if(fileSize >= 10) {
        fileSize= 10;
      }

      if(fileSize <= 3) {
        fileSize += 2;
      }

      if (fs.statSync(source + "/" + file).isDirectory()) {
        if (path.extname(file) != "") { //If the selected file is a directory
          //Remove dot from extention, and then append the file/directory
          extention = path.extname(file).split('.').join("");

          dataset.nodes.push({
            name: file,
            size: fileSize,
          });

        } else{
          dataset.nodes.push({
            name: file,
            size: fileSize,
          });
      }
      }

      if (fs.statSync(source + "/" + file).isFile()) {
        //Remove dot from extention, and then append the file/directory
        extention = path.extname(file).split('.').join("")

        dataset.nodes.push({
          name: file,
          size: fileSize,
        });
      }

      dataset.edges.push({
        source: superId,
        target: id,
      })

    });
    //stringafy data and then save the json in a file
    fs.writeFile("./object.json", JSON.stringify(dataset, null, 4), (err) => {
    if (err) {
        console.error(err);
        return;
    };

    });

    //Print data
    console.log(dataset.nodes)
    console.log(dataset.edges)

    };

    //GATHER DATA
    gatherData('/Users/enepoon');
    gatherData('/Users');
    gatherData('/Users/enepoon/Desktop');
    gatherData('/Users/enepoon/Desktop/datoteka');
    gatherData('/Users/enepoon/Desktop/computerinternals');

    //SETUP GRAPH
    function getRandom(min, max) {
      min = Math.ceil(min);
      max = Math.floor(max);
      return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    var svg = d3.select("#IntroPage")
        .append('svg')
            .attr('width', width)
            .attr('height', height)
        .append('g');

    //SET UP ZOOM FEATURES
    // function zoomed() {
    //   inner.attr('transform', 'translate(' + zoom.translate() + ')scale(' + zoom.scale() + ')');
    // }
    //
    // var zoom = d3.behavior.zoom()
    //   .scaleExtent([1, 8])
    //   .on("zoom", zoomed);
    //
    //
    //
    // var g = svg.append("g");
    //
    // svg
    //     .call(zoom)
    //     .call(zoom.event);
    //
    // svg.on("mouseup", function(){svg.call(zoom)});
    //
    // function zoomed() {
    //   g.attr("transform", "translate(" + zoom.translate() + ")scale(" + zoom.scale() + ")");
    // }

    //SETTING UP NEW NODES
    var update = function () {
        svg.selectAll("circle")
            .data(nodes) //.slice(1))
        .enter().append("circle")
            .attr("r", function (d) {
            return d.size;
        })
            .style("fill", function (d, i) {
            return color(i % 3);
        });
        force.on("tick", function() {
            edges
                .attr("x1", function(d) { return d.source.x; })
                .attr("y1", function(d) { return d.source.y; })
                .attr("x2", function(d) { return d.target.x; })
                .attr("y2", function(d) { return d.target.y; });

            nodes
                .attr("cx", function(d) { return d.x; })
                .attr("cy", function(d) { return d.y; });
          });
    };

    var addNewNode = function () {
        var newSize = 10;
        nodes.push({
          name: "null",
          isAFile: "yes",
          size: 20,
          path: source,
        });

        edges.push({

        });

        update();
        force.start();
    };

    var intervalId = null,
        pos = null;


    svg.on('mousedown', function () {
        //console.log('down');
        //addNewNode(this);
        pos = d3.mouse(this);
        intervalId = setInterval(addNewNode, 100);
    })
        .on('mouseup', function () {
        //console.log('up');
        clearInterval(intervalId);
    })
        .on("mousemove", function () {
        pos = d3.mouse(this);
        root.px = pos[0];
        root.py = pos[1];
        force.resume();
    })

    //SETTING UP FORCE DIRECTED GRAPH / PHYSICS

    var force = d3.layout.force() //Define force
        .nodes(dataset.nodes)
        .links(dataset.edges)
        .size([width, height])
        .linkDistance([50])
        .charge([-500])
        .start();

    //Graphing Data
    var canvas = svg
      .attr('width', width)
      .attr('height', height)
      // .call(zoom) // delete this line to disable free zooming
      // .call(zoom.event);

    var edges = canvas.selectAll("line")
      .data(dataset.edges)
      .enter()
          .append("line")
              .style("stroke", "#ccc")
              .style("stroke-width", 1)
              .style("weight", 1);
    var nodes = canvas.selectAll("circle")
        .data(dataset.nodes)
        .enter()
        .append("circle")
        .attr("r", function (d) { return (d.size); })
        .style("fill", function() {return d3.schemeCategory10[getRandom(0, 9)]})
    .call(force.drag);


    //Simulate the graph/map
    force.on("tick", function() {
        edges
            .attr("x1", function(d) { return d.source.x; })
            .attr("y1", function(d) { return d.source.y; })
            .attr("x2", function(d) { return d.target.x; })
            .attr("y2", function(d) { return d.target.y; });

        nodes
            .attr("cx", function(d) { return d.x; })
            .attr("cy", function(d) { return d.y; });
      });

  });
