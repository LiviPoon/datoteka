var elasticlunr = require('elasticlunr'),
    fs = require('fs');

var idx = elasticlunr(function () {
  this.setRef('id');

  this.addField('title');
  this.addField('tags');
  this.addField('body');
});

var doc1 = {
    "id": 1,
    "title": "Oracle released its latest database Oracle 12g",
    "body": "Yestaday Oracle has released its new database Oracle 12g, this would make more money for this company and lead to a nice profit report of annual year."
};

var doc2 = {
    "id": 2,
    "title": "Oracle released its profit report of 2015",
    "body": "As expected, Oracle released its profit report of 2015, during the good sales of database and hardware, Oracle's profit of 2015 reached 12.5 Billion."
};

idx.addDoc(doc1);
idx.addDoc(doc2);

fs.writeFile('example_index.json', JSON.stringify(idx), function (err) {
  if (err) throw err;
  console.log('done');
});

fs.readFile('example_data.json', function (err, data) {
  if (err) throw err;

  var raw = JSON.parse(data);

  var questions = raw.questions.map(function (q) {
    return {
      id: q.question_id,
      title: q.title,
      body: q.body,
      tags: q.tags.join(' ')
    };
  });

  questions.forEach(function (question) {
    idx.addDoc(question);
  });
});
