$("#search-bar").bind("enterKey",function(e){
  inputValue = document.getElementById("searchInput").value;
  var generalRegex = new RegExp(inputValue,"i"); //General regex for regular inputs
  var caseSensitiveRegex = new RegExp(caseSensitiveValue,"g"); //General Regex for case sensitive inputs

  var quotesRegex = new RegExp(/\"\w+\"/, "i"); //Regex for finding and identifying case-sensitive searches
  var braketsRegex = new RegExp(/\[\.\w+\]/, "i"); //Regex for finding and identifying required extentions
  var paranthesisRegex = new RegExp(/\(\w+\)/, "i"); //Regex for finding and itentifying required keywords

  var quotesRegexReturn = new RegExp((/\"\w+\"/), "i"); //Regex for grabing value inside case-sensitive searches.
  var braketsRegexReturn = new RegExp((/\[\.\w+\]/), "i"); //Regex for grabing value inside case-sensitive searches.

  if (quotesRegex.test(inputValue) == true) {
    caseSensitiveValue = quotesRegexReturn.test(inputValue);

    for (index = 0, index < files.length; index++) {
        if (generalRegex.test(files[index]) == true) {
          if (caseSensitiveRegex.test(files[index]) == true) {
            alert("case sensitive search found", files[index]);
          }
        }
      }
  }

  else if (braketsRegex.test(inputValue) == true) {
      extentionValue = braketsRegexReturn.test(inputValue);

      for (index = 0, index < files.length; index++){
          if (generalRegex.test(files[index]) == true) {
            if (caseSensitiveRegex.test(files[index]) == true) {
              alert("case sensitive search found", files[index]);
            }
          }
        }
  }

  else {
    for (index = 0; index < files.length; index++) {
      if (generalRegex.test(files[index]) == true) {
        alert(files[index]);
      }
    }
  }


  document.getElementById('searchInput').value = '';
  hideSearchBar();
});
