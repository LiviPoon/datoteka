$(document).ready(function(){
    var modal2= document.getElementById("advancedSearch");

    // Get the button that opens the modal
    var btn2 = document.getElementById("advancedSearchButton");

    // When the user clicks the button, open the modal
    btn2.onclick = function() {
      modal2.style.display = "block";
    }

    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function(event) {
      if (event.target == modal2) {
        modal2.style.display = "none";
      }
    }
  });
