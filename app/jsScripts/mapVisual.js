$(document).ready(function(){

  var w = window.innerWidth; //set the window's max and min height/width
  var h = window.innerHeight;

  var text_center = true;
  var outline = false;

  var min_score = 0; //controls score (color)
  var max_score = 15;

  var color = d3.scale.category20(); //set color scheme

  var size = d3.scale.pow().exponent(1) //set size
    .domain([1,100])
    .range([8,24]);

  var force = d3.layout.force() //declare d3 force
    .linkDistance(60)
    .charge(-1000)
    .size([w,h]);

  var default_node_color = "#ccc"; //declaring color schemes, and sizes for map
  var default_link_color = "#888";
  var nominal_base_node_size = 12;
  var nominal_text_size = 10;
  var max_text_size = 24;
  var nominal_stroke = 1;
  var max_stroke = 4.5;
  var max_base_node_size = 15;
  var min_zoom = 0.1;
  var max_zoom = 7;
  var svg = d3.select("#map-screen").append("svg");
  var zoom = d3.behavior.zoom().scaleExtent([min_zoom,max_zoom])
  var g = svg.append("g");

  var hoverOverNode = false; //to tell if a context menu can pop up

  svg.style("cursor","move");

  $("#map-screen").bind("contextmenu", function(e) {
    if (hoverOverNode == true) {
      $("#nodeDropDownMenu").show();
      var menu = document.getElementById('nodeDropDownMenu');
      menu.style.position = 'absolute';
      menu.style.top = e.clientY + 'px';
      menu.style.left = e.clientX + 'px';
    };
  });

  $("#properties").bind("click", function(e) {
    var propmenu = document.getElementById('propertiesMenu');


  });

  $(document).mouseup(function(e)
  {
      var container = $("#nodeDropDownMenu");

      // if the target of the click isn't the container nor a descendant of the container
      if (!container.is(e.target) && container.has(e.target).length === 0)
      {
          container.hide();
      }
  });

  function mouseUp() {
      window.removeEventListener('mousemove', move, true);
  }

  function mouseDown(e) {
      window.addEventListener('mousemove', move, true);
  }

  // $("#map-screen").bind('contextmenu click',function(){
  //   $("#nodeDropDownMenu").hide();
  // });


  function getRandom(min, max) { //define getRandom function for random color generation
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }

  d3.json("object5.json", function(error, graph) { //grab data and set it to variable graph
    function render() {
      var linkedByIndex = {}; //connect all the nodes
          graph.links.forEach(function(d) {
      	linkedByIndex[d.source + "," + d.target] = true;
          });

      	function isConnected(a, b) { //check if they are all connnected
              return linkedByIndex[a.index + "," + b.index] || linkedByIndex[b.index + "," + a.index] || a.index == b.index;
          }

      	function hasConnections(a) { //check if they have any other connections
      		for (var property in linkedByIndex) {
      				s = property.split(",");
      				if ((s[0] == a.index || s[1] == a.index) && linkedByIndex[property]){
                return true;
              }
      		}
      	return false;
      	}

        force //call force on the nodes and links
          .nodes(graph.nodes)
          .links(graph.links)
          .start();

        var link = g.selectAll(".link") //declare link attributes
          .data(graph.links)
          .enter().append("line")
          .attr("class", "link")
      	.style("stroke-width",nominal_stroke)
      	.style("stroke", function() {return d3.scale.category20(getRandom(0, 9))})

        var node = g.selectAll(".node") //declare node attributes
          .data(graph.nodes)
          .enter().append("g")
          .attr("class", "node")
        //  .call(force.drag)


      	node.on("dblclick.zoom", function(d) { d3.event.stopPropagation(); //transform graph based on the if you scroll
      	var dcx = (window.innerWidth/2-d.x*zoom.scale());
      	var dcy = (window.innerHeight/2-d.y*zoom.scale());
      	zoom.translate([dcx,dcy]);
      	 g.attr("transform", "translate("+ dcx + "," + dcy  + ")scale(" + zoom.scale() + ")");
      	});
      	var tocolor = "fill";
      	var towhite = "stroke";
      	if (outline) {
      		tocolor = "stroke"
      		towhite = "fill"
      	}

        var circle = node.append("path") //define circle
            .attr("d", d3.svg.symbol()
              .size(function(d) { return Math.PI*Math.pow(size(d.size)||nominal_base_node_size,2); })
              .type(function(d) { return d.type; }))

      	.style(tocolor, function(d) {
      	if (isNumber(d.score) && d.score>=0) return color(d.score);
      	else return default_node_color; })
          //.attr("r", function(d) { return size(d.size)||nominal_base_node_size; })
      	.style("stroke-width", nominal_stroke)
      	.style(towhite, "white");

        var text = g.selectAll(".text") //define text/labels on the map
          .data(graph.nodes)
          .enter().append("text")
          .attr("x", "3em")
      	.style("font-size", nominal_text_size + "px")

      	if (text_center)
      	 text.text(function(d) { return d.id; })
      	.style("text-anchor", "middle");
      	else
      	text.attr("dx", function(d) {return (size(d.size)||nominal_base_node_size);})
          .text(function(d) { return '\u2002'+d.id; });

        node.on("mouseover", function (d) {
            hoverOverNode = true;
            // var g = d3.select(this); // The node
            // var div = d3.select("body").append("div")
            //         .attr('pointer-events', 'none')
            //         .attr("class", "tooltip")
            //         .style("opacity", 1)
            //         .html(d.id)
            //         .style("left", (d.x + 25 + "px"))
            //         .style("top", (d.y + 15 + "px"))


      } ).on("mousedown", function(d) { d3.event.stopPropagation();

      }	).on("mouseout", function(d) {
             d3.select("body").select('div.tooltip').remove();
             hoverOverNode = false;
        });


        zoom.on("zoom", function() {

          var stroke = nominal_stroke;
          if (nominal_stroke*zoom.scale()>max_stroke) stroke = max_stroke/zoom.scale();
          link.style("stroke-width",stroke);
          circle.style("stroke-width",stroke);

      	var base_radius = nominal_base_node_size;
          if (nominal_base_node_size*zoom.scale()>max_base_node_size) base_radius = max_base_node_size/zoom.scale();
              circle.attr("d", d3.svg.symbol()
              .size(function(d) { return Math.PI*Math.pow(size(d.size)*base_radius/nominal_base_node_size||base_radius,2); })
              .type(function(d) { return d.type; }))

      	//circle.attr("r", function(d) { return (size(d.size)*base_radius/nominal_base_node_size||base_radius); })
      	if (!text_center) text.attr("dx", function(d) { return (size(d.size)*base_radius/nominal_base_node_size||base_radius); });

      	var text_size = nominal_text_size;
          if (nominal_text_size*zoom.scale()>max_text_size) text_size = max_text_size/zoom.scale();
          text.style("font-size",text_size + "px");

      	g.attr("transform", "translate(" + d3.event.translate + ")scale(" + d3.event.scale + ")");
      	});

        svg.call(zoom);

        resize();

        force.on("tick", function() {

          node.attr("transform", function(d) { return "translate(" + d.x + "," + d.y + ")"; });
          text.attr("transform", function(d) { return "translate(" + d.x + "," + d.y + ")"; });

          link.attr("x1", function(d) { return d.source.x; })
            .attr("y1", function(d) { return d.source.y; })
            .attr("x2", function(d) { return d.target.x; })
            .attr("y2", function(d) { return d.target.y; });

          node.attr("cx", function(d) { return d.x; })
            .attr("cy", function(d) { return d.y; });
      	});
      };

      function enterNodes(n) {
        var g = n.enter().append("g")
          .attr("class", "node");

        g.append("circle")
          .attr("cx", 0)
          .attr("cy", 0)
          .attr("r", function(d) {return d.weight})
          .call(force.drag);

        g.append("text")
          .attr("x", function(d) {return d.weight + 5})
          .attr("dy", ".35em")
          .text(function(d) {return d.key});
      }

      function exitNodes(n) {
        n.exit().remove();
      }

      function enterLinks(l) {
        l.enter().insert("line", ".node")
          .attr("class", "link")
          .style("stroke-width", function(d) { return d.weight; });
      }

      function exitLinks(l) {
        l.exit().remove();
      }

      function update() {
        force.nodes(graph.nodes).links(graph.links);

        var l = svg.selectAll(".link")
          .data(graph.links, function(d) {return d.source + "," + d.target});
        var n = svg.selectAll(".node")
          .data(graph.nodes, function(d) {return d.key});
        enterLinks(l);
        exitLinks(l);
        enterNodes(n);
        exitNodes(n);
        render();
      }

      function resize() {
        var width = window.innerWidth, height = window.innerHeight;
    	svg.attr("width", width).attr("height", height);

    	force.size([force.size()[0]+(width-w)/zoom.scale(),force.size()[1]+(height-h)/zoom.scale()]).resume();
        w = width;
    	h = height;
    	}

      function resize() {
        var width = window.innerWidth, height = window.innerHeight;
    	svg.attr("width", width).attr("height", height);

    	force.size([force.size()[0]+(width-w)/zoom.scale(),force.size()[1]+(height-h)/zoom.scale()]).resume();
        w = width;
    	h = height;
    	}

      function isNumber(n) {
        return !isNaN(parseFloat(n)) && isFinite(n);
      }

      render();
      update();

    });
  });
