// This js script controls the indexing and capturing of local data, and also holds the code controling the search bar.

//recursion
let directories = [];
let files = [];

let localIndex = [];
let remoteIndex = []; //create a new Index

let ran = "true"; //creating a variable that lets the app know wether or not recursion has ran

var superId = 0; //set variables for indexing
var id = 0;
var dataset = { //json object holding info for building the map
    nodes: [
    ],
    links: [
    ]
};

function showSearchBarResults() { //show the results from a search
  $("#searchResults").delay(500).fadeIn();
}

function getFiles(fileSource){  // define the return structure
    superId += 1; // define super ID
    dataset.nodes.push({ //push super
      size:50,
      score:0,
      id: path.basename(fileSource),
      type: "circle",
    })

    fs.readdirSync(fileSource).filter(file => file[0] !== ".").forEach(file => {
        id+=1// for every file in this directory
        stats = fs.statSync(fileSource + "/" + file); //grab the states of the current file

        files.push(file); //save files in a seperate array.

        var fileINDEX = { //create a seperate json object to hold info for the search engine (the index)
          "id": id,
          "title": file,
          "path": fileSource + "/" + file
        }

        remoteIndex.push(fileINDEX); //push stuff to the index

        var fileSize = stats["size"]/100; //find the size of file

        if(fileSize >= 100) { //normalize sizes
          fileSize= 100;
        }

        else if (fileSize <= 3) {
          fileSize += 2;
        }

        let fullpath = fileSource + "/" + file; // the full path to this file
        try { // wrap in a try block in case statSync throws
            if (fs.statSync(fullpath).isDirectory()) { // if it's a directory
                dataset.nodes.push(getFiles(fullpath)); // get everything in the directory and add that as a child
            } else { // it's a file
                dataset.nodes.push({ //push file as a node
                  size:fileSize,
                  score:0,
                  id: file,
                  type: "circle",
                })

                dataset.links.push({ //push file on to super
                  source: superId,
                  target: id,
                })
            }
        } catch (e) {
            console.error(e);
        };
    });

    return dataset;
  };

$.each(dataset, function(key, value){
    if (value === "" || value === null){
        delete dataset[key];
    }
});


function initialize() { //function to index computer one time
    fs.readFile('jsScripts/indexInitial.txt', (error, string) => {
      if (string.toString() == "true") {
        fs.writeFile("testobject.json", JSON.stringify(dataset, getCircularReplacer()), (err) => {
            if (err) {   //send data to json file for graphing
                console.error(err);
                return;
              };

          });

      };

      if (string.toString() !== "true"){
        getFiles("/Users/enepoon/desktop");
        writeIndex();
        checkIndex();
        fs.writeFile('jsScripts/indexInitial.txt', ran, (err) => {
            if (err) throw err;
            console.log('The file has been saved!');
          });
        };
      });
    };

function writeIndex() { //write the index
  fs.writeFile('jsScripts/index.json', localIndex, (err) => {
      if (err) throw err;
      console.log('The file has been saved!');
    });
};

function checkIndex() { //check if the remoteIndex is the same as the local index
  statment = JSON.stringify(remoteIndex) === JSON.stringify(localIndex);
  if (statement == "true") {
    //do nothing
  }

  if (statement == "false") {
    writeIndex(); //write a new index
  }
};

getFiles("/Users/enepoon/desktop"); //grab the files from a local directory

//NOTE need to make it so that it can grab any computer, and not just my own

initialize(); //initialize the indexes

const getCircularReplacer = () => { //remove circular objects from the node map json object
  const seen = new WeakSet();
  return (key, value) => {
    if (typeof value === "object" && value !== null) {
      if (seen.has(value)) {
        return;
      }
      seen.add(value);
    }
    return value;
  };
};

let miniSearch = new MiniSearch({ //create a new search object
  fields: ['title', 'path'], // fields to index for full-text search
  storeFields: ['title', 'path'] // fields to return with search results
})

miniSearch.addAll(remoteIndex); //add the index into the searches dataset

$("#search-bar").keyup(function(e){ //bind the enter to the search bar div
  if(e.keyCode == 13)
  {
    $(this).trigger("enterKey");
  }
});

var searchBar = document.getElementById('search-bar'); //grab search-bar and assign eventlisteners
searchBar.addEventListener('mousedown', mouseDown, false);

var properties = document.getElementById('properties');
properties.addEventListener('mouseDown', mouseDown, false);

window.addEventListener('mouseup', mouseUp, false);

function mouseUp() {
    window.removeEventListener('mousemove', move, true);
}

function mouseDown(e) {
    window.addEventListener('mousemove', move, true);
}

function move(e) { //move the search bar without removing the option to enter the input
    searchBar.style.top = e.clientY + 'px';
    searchBar.style.left = e.clientX + 'px';
    searchBar.style.transform = e.clientX + "%", e.clientY + "%";

    properties.style.top = e.clientY + 'px';
    properties.style.left = e.clientX + 'px';
    properties.style.transform = e.clientX + "%", e.clientY + "%";
};

$("#search-bar").bind("enterKey",function(e){ //tell the program what to do when pressing the enter key

  showSearchBarResults(); //show results

  event.preventDefault(); //animate it getting bigger after showing results
    $("#search-bar").animate({
      width : 600,
      height : 400
    }, 400);

    $(".customSize.searchBar").animate({
      width : 580
    }, 400);

    inputValue = document.getElementById("searchInput").value; //get the search input

    let results = miniSearch.search(inputValue); //get the results

    for (i = 0; i <= 10; i++) {
      $("#searchResult" + i).html(results[i].title + " -- " +results[i].path); //make the results look nice
    };

    document.getElementById('searchInput').value = '';
    // hideSearchBar();
});
