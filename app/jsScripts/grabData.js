 //code that is subject to removal
const fs = require('fs');
const path = require("path");
let directories = [];
let files = [];
var id = 0;
var superId = -1;
var source;

var dataset = {
    nodes: [
    ],
    links: [
    ]
};

function gatherData(source) {
  superId += 1;
  var source = source;

  dataset.nodes.push({
    size: 50,
    score:0,
    id: path.basename(source),
    type: "circle",

  })

  fs.readdirSync(source).forEach(file => {
    id += 1;
    stats = fs.statSync(source + "/" + file);

    var selectedfile = file;
    var fileName = selectedfile.toUpperCase();
    var length = 5;
    var trimmedFileName = fileName.substring(0, length);

    //check if the file is bigger than 10, if so
    var fileSize = stats["size"]/100;

    if(fileSize >= 100) {
      fileSize= 50;
    }

    if(fileSize <= 3) {
      fileSize += 2;
    }

    if (fs.statSync(source + "/" + file).isDirectory()) {
      if (path.extname(file) != "") { //If the selected file is a directory
        //Remove dot from extention, and then append the file/directory
        extention = path.extname(file).split('.').join("");

        dataset.nodes.push({
          size: fileSize,
          score: fileSize,
          id: trimmedFileName,
          type: "circle",
        });

      } else{
        dataset.nodes.push({
          size: fileSize,
          score: fileSize,
          id: trimmedFileName,
          type: "circle",
        });
    }
    }

    if (fs.statSync(source + "/" + file).isFile()) {
      //Remove dot from extention, and then append the file/directory
      extention = path.extname(file).split('.').join("")

      dataset.nodes.push({
        size: fileSize,
        score: fileSize,
        id: trimmedFileName,
        type: "circle",
      });
    }

    dataset.links.push({
      source: superId,
      target: id,
    })

  });
  //stringafy data and then save the json in a file
  fs.writeFile("object5.json", JSON.stringify(dataset, null, 4), (err) => {
  if (err) {
      console.error(err);
      return;
     };
  });

};

gatherData('/Users/enepoon');
gatherData('/Users');
gatherData('/Users/enepoon/Desktop');
gatherData('/Users/enepoon/Desktop/datoteka');
gatherData('/Users/enepoon/Desktop/computerinternals')
