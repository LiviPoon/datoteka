const fs = require('fs');
const util = require('util');
const path = require("path");
const ExifImage = require('exif').ExifImage;

var concepts = [];
var prediction = [];
var metaData = [];
var make = [];
var err = "";

function main(filesource){
  fs.readdirSync(filesource).forEach(file => {
    filePath = filesource + "/" + file;
    try {
      new ExifImage({ image : filePath }, function (error, exifData) {
          if (error) {
            console.log('Error: '+error.message);
          }

          else{
              var date = exifData['image']['ModifyDate'];
              var regex = /\d{4}\D\d..\d{2}/g;
              var snippedDate = date.match(regex);
              var make = exifData['image']['Make'];

              if (!fs.existsSync("./" + make)){
                fs.mkdirSync("./" + make);
              }

              newFilePath = "./" + make + "/" + file;
              fs.rename(filePath, newFilePath, ()=> {
                  console.log("\nFile Renamed!\n");
            });
          };
      });
    }

    catch (error) {
        console.log('Error: ' + error.message);
      }
    });
  };



main("/Users/enepoon/Desktop/datoteka./app/jsScripts/exifSorting/exifTests");
