const MiniSearch = require("minisearch"), //create a mini search object
  fs = require('fs'),
  util = require('util'),
  path = require("path");

var optionBackLocation = ""; //assign a variable to keep track of where to go back to when on the options screen

//define various titles to animate using TypeIt
let mainScreenText = new TypeIt(".main-screen-title", {
  strings: "datoteka.",
  speed: 150,
  startDelay: 4500
});

let optionScreenText = new TypeIt(".options-screen-title", {
  strings: "options.",
  speed: 150,
  startDelay: 2000
});

let aboutScreenText = new TypeIt(".about-screen-title", {
  strings: "about.",
  speed: 150,
  startDelay: 2000
});

let videoOptionScreenText = new TypeIt(".options-video-screen-title", {
  strings: "video.",
  speed: 150,
  startDelay: 2000
});

let loadingScreenText = new TypeIt("#loading-word",{
  strings: "<em>'We have persistant objects, they're called files.</em> - Ken Thompson Co-Inventor of Go",
  speed: 50,
});

//require electron
const remote = require('electron').remote
$('#quit').on('click', e => {
    process.exit();
})

$(document).ready(function(){ //when the document is ready run the following code
  var screens = [];

  function showSplashScreen(){ //assign functions to show and hide certain screens
    $("#splash-screen").show().delay(7500).fadeOut(); //set a delay
    loadingScreenText.reset(); //reset the typeit script
    loadingScreenText.go(); //run the typeit script
  }

  function showOptionsVideoScreen(){
    $("#options-video-screen").hide().delay(1500).fadeIn();
    videoOptionScreenText.reset();
    videoOptionScreenText.go();
  }

  function hideOptionsVideoScreen(){
    $("#options-video-screen").delay(1500).fadeOut();
  }

  function showPrivacyScreen() {
    $("#options-privacy-screen").delay(1500).fadeIn();
  }

  function hidePrivacyScreen() {
    $("#options-privacy-screen").delay(1500).fadeOut();
  }

  function showMapScreen() {
    $("#map-screen").hide().delay(1500).fadeIn();
    //10000 fade in with splashscreen toggled
  }


  function hideMapScreen() {
    $("#map-screen").delay(1500).fadeOut();
  }

  function showHelpScreen() {
    $("#help-screen").delay(1500).fadeIn();
  }

  function hideHelpScreen() {
    $("#help-screen").delay(1500).fadeOut();
  }

  function showAboutScreen() {
    $("#about-screen").delay(1500).fadeIn();
    aboutScreenText.reset();
    aboutScreenText.go();
  }

  function hideAboutScreen() {
    $("#about-screen").delay(1500).fadeOut();
  }

  function showOptionsScreen() {
    $("#options-screen").delay(1500).fadeIn();
    optionScreenText.reset()
    optionScreenText.go();
  }

  function hideOptionsScreen() {
    $("#options-screen").delay(1500).fadeOut();
  }

  function showDeveloperScreen() {
    $("#developers-screen").delay(1500).fadeIn();
  }

  function hideDeveloperScreen() {
    $("#developers-screen").delay(1500).fadeOut();
  }

  function showMainScreen() {
    $("#main-screen").delay(1500).fadeIn();
    mainScreenText.reset();
    mainScreenText.go();
  }

  function hideMainScreen() {
    $("#main-screen").fadeOut();
  }

  function showSearchBar() {
    $("#search-bar").delay(500).fadeIn();
  }

  function hideSearchBar() {
    $("#search-bar").delay(500).fadeOut();
  }

  function hideSearchBarResults() {
    $("#searchResults").delay(500).fadeOut();
  }

  function hideContextMenu() { //context menu stuff is in mapvisual.js
    $("#nodeDropDownMenu").delay(500).fadeOut();
  }

  function showPropertiesMenu() {
    $("#propertiesMenu").delay(500).fadeIn();
  }

  function hidePropertiesMenu() {
    $("#propertiesMenu").delay(500).fadeOut();
  }

  //Hide all the screens
  $("#splash-screen").hide();
  $("video-screen").hide();
  $("#map-screen").hide();
  $("#help-screen").hide();
  $("#about-screen").hide();
  $("#options-screen").hide();
  $("#developers-screen").hide();
  $("#options-video-screen").hide();
  $("#options-privacy-screen").hide();
  $("#searchBar").hide();
  $("#contextmenu").hide();
  $("#propertiesMenu").hide();
  //Start initialization process
  $("#main-screen").hide().delay(5000).fadeIn();
  $("#bootup-screen").show().delay(5000).fadeOut();
  mainScreenText.reset();
  mainScreenText.go();
  screens.unshift("#main-screen");

//Mainmenu Buttons
  $("#begin").click(function(){
    $(screens[0]).fadeOut(); //fade out previous screen
    screens.unshift("#map-screen"); //make sure that the current screen is the map screen
    hideMainScreen(); //hide the mainmenu
    // showSplashScreen(); //show the splash screen
    showMapScreen(); //show the map screen
  });

  $("#options").click(function(){
    $(screens[0]).fadeOut();
    screens.unshift("#options-screen");
    optionBackLocation = "mainscreen";

    showOptionsScreen();
    hideMainScreen();
  });

  $("#developers").click(function(){
    $(screens[0]).fadeOut();
    screens.unshift("#devleopers-screen");
    showDeveloperScreen();
    hideMainScreen();
  })

  $("#about").click(function(){
    $(screens[0]).fadeOut();
    screens.unshift("#about-screen");
    showAboutScreen();
    hideMainScreen();
  });

  $("#mainMenuButton").click(function(){
    $(screens[0]).fadeOut();
    screens.unshift("#main-screen");
    showMainScreen();
  });

//Options Buttons
  $("#video").click(function(){
    screens.unshift("#options-video-screen");
    hideOptionsScreen();
    showOptionsVideoScreen();
  });

  $("#privacy").click(function(){
    screens.unshift("#options-video-screen");
    hideOptionsScreen();
    showPrivacyScreen();
  });

//Map Buttons
  $("#mainMenuButton").click(function(){
    $(screens[0]).fadeOut();
    hideSearchBar();
    hidePropertiesMenu();
    hideContextMenu();
    hideMapScreen();
    screens.unshift("#main-screen");
    showMainScreen();
  });

  $("#aboutButton").click(function(){
    $(screens[0]).fadeOut();
    hideSearchBar();
    hidePropertiesMenu();
    hideMapScreen();
    screens.unshift("#about-screen");
    showAboutScreen();
  });

  $("#optionsButton").click(function(){
    $(screens[0]).fadeOut();
    hideSearchBar();
    hideMapScreen();
    hidePropertiesMenu();
    screens.unshift("#options-screen");
    showOptionsScreen();
    optionBackLocation = "mapscreen";
  });

  $("#homeButton").click(function(){
    $(screens[0]).fadeOut();
    hideSearchBar();
    hideMapScreen();
    hidePropertiesMenu();
    screens.unshift("#main-screen");
    showMainScreen();
  });

  function showSearchBarResults() {
    $("#searchResults").delay(500).fadeIn();
  }

  $("#searchButton").click(function(){
    showSearchBar();
    showSearchBarResults();
  });

  $("#searchButtonMinor").click(function(){
    showSearchBar();
    showSearchBarResults();
  });

//Map contextmenu buttons

  $("#properties").click(function(){
    showPropertiesMenu();
    hideContextMenu();
    $('#propertiesTitle').html("properties")
    $('#properties-content').html("");

  })

  //propertiesMenu Buttons

  $("#properies-back").click(function(){
    hidePropertiesMenu();
  })

//Map contextmenu buttons continued
  $("#cut").click(function(){
  })

  $("#copy").click(function(){
  })

  $("#paste").click(function(){
  })

  $("#delete").click(function(){
  })



//Back Buttons

  $("#options-back").click(function(){

    if(optionBackLocation == "mainscreen") {
      hideOptionsScreen();
      showMainScreen();
    }

    if(optionBackLocation == "mapscreen") {
      hideOptionsScreen();
      showMapScreen();
    }

    screens.unshift("#options-screen");
    });

  $("#privacy-back").click(function(){
    hidePrivacyScreen();
    showOptionsScreen();
    screens.unshift(screens[0]);
  });

  $("#about-back").click(function(){
    hideAboutScreen();
    $(screens[0]).fadeOut();
    $(screens[1]).delay(1500).fadeIn();
    screens.unshift(screens[0]);
  });

  $("#developers-back").click(function(){
    $(screens[0]).fadeOut();
    $(screens[1]).delay(1500).fadeIn();
    screens.unshift(screens[0]);
  });

  $("#options-video-back").click(function(){
    $(screens[0]).fadeOut();
    $(screens[1]).delay(1500).fadeIn();
    screens.unshift(screens[0]);
  });




});
