anime ({
  targets: "#login-svg path",
  strokeDashoffset: [anime.setDashoffset, 0],
  easing: 'easeInOutSine',
  duration: 5000,
  direction: 'alternate',
  loop: true
})
