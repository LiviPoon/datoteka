const electron = require("electron");
const app = electron.app;
const BrowserWindow = electron.BrowserWindow;
//const {app, BrowserWindow} = require('electron')
const path = require('path')
const url = require('url')
const Menu = electron.Menu;
const MenuItem = electron.MenuItem

const { globalShortcut, webFrame } = require('electron')

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
//Turns of Dev Warnings
delete process.env.ELECTRON_ENABLE_SECURITY_WARNINGS;
process.env.ELECTRON_DISABLE_SECURITY_WARNINGS = true;

function createWindow () {
  // Create the browser window.
  win = new BrowserWindow({
    width: 1920,
    height: 1080,
    webPreferences: {
      nodeIntegration: true
    },
    frame: false,
    titleBarStyle: 'hiddenInset',
  })

  // and load the index.html of the app.
  win.loadFile('index.html');
  // Emitted when the window is closed.
  win.on('closed', () => {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    win = null
  })
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', function(){
  createWindow()
   // Mac App menu - used for styling so shortcuts work
   if (process.platform === 'darwin') {

    const template = [
      {
        label: 'Datoteka' , submenu: [
        { role: 'about' },
        { type: 'separator' },
        { role: 'services' },
        { type: 'separator' },
        { role: 'hide' },
        { role: 'hideothers' },
        { role: 'unhide' },
        { type: 'separator' },
        { role: 'quit' }
        ],
      },

      {
        label: 'File', submenu: [

          { label: 'Create New Map',
          accelerator: 'Shift + Return'},

          { label: 'New Child Node',
          accelerator: 'Tab'},


          { label: 'New Sibling Node',
          accelerator: 'CommandOrControl + Shift + Return'},

          { label: 'New Parent Node',
          accelerator: 'Option + Alt + Tab'},
        ],
      },

      {
        label: 'Edit', submenu: [
          { label: 'Copy',
          accelerator: 'CommandOrControl + C'},

          { label: 'Paste',
          accelerator: 'CommandOrControl + P'},

          { label: 'Delete',
          accelerator: 'Delete'},

          { label: 'Edit Title',
          accelerator: 'CommandOrControl + Return'},

          { label: 'Edit Node Link',
          accelerator: 'CommandOrControl + Shift + K'},

          { label: 'Create Comment/Note',
          accelerator: 'CommandOrControl + Shift + C'},

          { label: 'Sort Alphabetically',
          accelerator: 'CommandOrControl + G'},

          { label: 'Copy Single Node',
          accelerator: 'CommandOrControl + Option + Shift + C'},

          { label: 'Create New Connection',
          accelerator: 'CommandOrControl + Shift + L'},

          { label: 'Reorganize Nodes',
          accelerator: 'Option + Alt + CommandOrControl + R'},
        ]
      },

      {
        label: 'View', submenu: [
          { label: 'Zoom In',
          accelerator: 'CommandOrControl + numadd'},

          { label: 'Zoom Out',
          accelerator: 'CommandOrControl + numsub'},

          { label: 'Fit to Window',
          accelerator: 'CommandOrControl + ='},

          { label: 'Search',
          accelerator: 'CommandOrControl + F'},
        ]
      },

      {
        label: 'Search', submenu: [
          { label: 'Quick Search',
          accelerator: 'CommandOrControl + F + Space'},

          { label: 'Advanced Search',
          accelerator: 'CommandOrControl + Shift + Space'},
        ]
      },

      {
        label: 'History',
      },


      {
        label: 'Help', submenu: [
          { role: 'reload' },
          { role: 'toggleFullScreen' },
          { role: 'toggleDevTools' },
        ],
      },
    ];
    const menu = Menu.buildFromTemplate(template);
    Menu.setApplicationMenu(menu);
};
});

app.on('will-quit', () => {
  // Unregister all shortcuts.
  globalShortcut.unregisterAll()
})

// Quit when all windows are closed.
app.on('window-all-closed', () => {
  // On macOS it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') {
    app.quit()
  }
})
app.on('activate', () => {
  // On macOS it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (win === null) {
    createWindow()
  }
})

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.
