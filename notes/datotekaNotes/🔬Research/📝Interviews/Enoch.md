# Enoch
As a Archivist and a Journalist, Enoch needs a way to quickly access files, photos, and notes; so that he can meet his deadlines.

While processing more than 12g's of data, Enoch needs an optimized search bar so that he can easily search for what he needs.

[[User Interviews]]