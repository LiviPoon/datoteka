# Rene
As a UX Designer, Rene needs his organization tools to be very customizable, with separate screens and separate tabs so that he has more freedom to how he can organize his files; and so that he is not stuck with one way of doing things.

When searching for a particular file, Rene wants to be able to have more refined parameters that he can set in his search so that he can search for the particular file he wants.

[[User Interviews]]