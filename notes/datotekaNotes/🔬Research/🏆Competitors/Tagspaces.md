### Tagspaces

#### Notable from the website

 TagSpaces products can be used as alternative for Evernote's note taking and web scrapping functionalities.

 TagSpaces does not persists the tags in a centralized way. As a consequence, the added meta information is not vendor locked. The absence of a database, makes syncing of it easy across different devices with services like Dropbox or Nextcloud.


#### Pros

>Very Customizable
Integrated Tag System
Supports multiple types of files
Open Sourced
Offline and Online Capabilities
Cross-platform (Mac, Linux, Windows, Android, IOS, Chrome, Firefox)
No databases
No vendor locking
Continuous support
Unobtrusive
Compatible with cloud services for synchronization of files
Pro - Ability to connect with AWS S3 buckets

#### Cons
>Doesn't help to Categorize Files or Pictures
Complicated Process of Reorganizing
Basic Search Capabilities - Tag/Keyword
Similar Capabilities with Evernote
Limited Personalization Methods for Creative Users

[[Competitors]]