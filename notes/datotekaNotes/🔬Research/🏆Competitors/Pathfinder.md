### Pathfinder

#### Pros

>Dropbox integration
Modules everywhere
FolderSync
Folder Merging
Arranging and Grouping items
Editable Path Navigator
One-Click Dual Pane Copy
Secure Delete
File Transfer Queue
ACL Editor - Create and modify Access Control Lists, an advanced alternative to macOS's standard Unix permissions.
Calculate File Checksums
File Tagging
Simultaneous Batch Renaming
Hex Editor - Low-level file editing without ever leaving your browser
Drop Stack - Freeze drag-and-drop operations by placing files into a temporary stack.
Dual-pane Browser
Tabs & Bookmarks
Low-level Search
File List Filtering
Smart Sorting - Sort by folders, packages, or files first
Advanced Selection
Keyboard Access
Quick Look & Cover Flow
Integrated Terminal
Command Line Tools
Source Control - Run common Git and Subversion commands without the command line
Application Launcher
Archives
Text & Image Editors

#### Cons

>Very Complicated
Low Level Search Capabilities
Smart Sorting Capabilities are Limited

[[Competitors]]