### Astro File Manager
#### Notable Quotes from the website

> Connect all your storages in one place - SD Card, internal memory, cloud services like Dropbox, Google Drive, Facebook, Box, Microsoft OneDrive, or local networks - PC, Mac, and Linux

 >Mark files and folders as hidden to secure sensitive information. Compress and extract files in Zip (WinZip) and RAR (WinRAR) formats. Download and open files from the web.


#### Pros

>Cloud Storage / File Transfer
Files are Accessible - Muilti Platform
Allows you to Compress and Extract files
Able to look at specific file types
Cons

>Doesn't help to Categorize Files or Pictures
Complicated Process of Reorganizing
Basic Organization Capabilities - Folders

[[Competitors]]