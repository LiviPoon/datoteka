### Tabbles
#### Notable Quotes from the website

 Let Tabbles automatically create tags and tag your files based on their path, their name and their content or data-mine information using regular expressions.

 Tabbles unified search box can search through file name and paths, as well as tags, comments and file content.

#### Pros

>Cloud Storage / File Transfer
Files are Accessible - Muilti Platform
Allows you to Compress and Extract files
Able to look at specific file types
Data Mining - Auto-Tagging
Wide Range Search - Tags, Comments and file content
Heigharchy Tag System

#### Cons

>Doesn't use ML or AI to organize files - Manual Control
Complicated Process of Reorganizing
Basic Organization Capabilities - Folders

[[Competitors]]

