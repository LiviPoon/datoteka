# Redeveloping File Organizers

## General Problem: People need a simple easy way to organize, sort, and access files, links, and photos.

Solutions - For Specific Problems

An app that stores your links, photos, and files. Similar to a browser you can search with it what you need, for example you need stuff on programming, and it will come up with answers relating to that search.

A photo organizer that uses image recognition to parse out what goes in what directory, users then are able to choose what categorization works best for them.

Possible Languages

>PHP with HHVM (Fast Virtual Machine designed for PHP and Hack)
Python
Possible Tools/APIs
Clarifai - Image Recognition API (Python, Java, JS, ObjC, C#, PHP, cURL)
Drive API - Google Drive API (Java, Python, PHP, NET, Ruby, Node.JS, Obj C)
Pyautogui - Library to control a computer's mouse (Python)
Research


[[Astro File Manager]]
[[Pathfinder]]
[[Tabbles]]
[[Tagspaces]]

[[🌎Research]]



