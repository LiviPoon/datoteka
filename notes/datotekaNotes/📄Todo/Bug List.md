# **Bug List**



- [ ] VISUAL BUG - Returning to homescreen from map screen #important
- [ ] VISUAL BUG - Searchbar showing response divs before search
- [ ] VISUAL BUG - Node labling locations overlap nodes
- [ ] VISUAL BUG - When calling update function, old nodes just group up into the top left corner.
- [ ] BUG - Back button is not working in about screen
- [ ] BUG - Back button is not working in privacy screen


## *Fixed Bugs*
- [x] Button system loop from Options -> Main Screen -> Options
- [x] Button system loop from Options -> Video -> Options
- [x] VISUAL BUG - Context menu popping up where mouse is in the middle, not the corner.

[[Datoteka.]]