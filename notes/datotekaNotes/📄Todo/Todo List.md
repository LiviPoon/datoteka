# **Todo**

---
- [ ] Create a working search bar. #important
	- [ ] Get basic functionality working
	- [ ] Get complex functionality (using different commands like force search capabilities and searching by tasks)
---
- [X] Finish Regression #important
	- [X] Find a way to optimize storage
	- [X] Fix errors thrown when reading and parsing the JSON Object
---
- [ ] Finishing Indexing #important
	- [X] Indexing every
	- [X] Saving index, and updating every login
	- [X] Saving index, and replacing old with new index
	- [ ] Saving index, and replacing every time a file is changed
	- [X] Saving index, but not running recursion a lot.

---
- [ ] Finish CSS #lessimportant

---
- [ ] Finalize Button System #important
	- [x] Delete Everything
	- [x] Rewrite new system
	- [x] Test and make sure new system is in place and is working when code is augmented
	- [ ] Finish Button System
---
- [ ] Finish Additional Map Features #important
	- [X] Context Menus
	- [ ] Popup Menus
	- [ ] Tags
	- [ ] Sub Menu Focusing
	- [ ] Help Screen

---
- [ ] Manipulation of Nodes #important
	- [ ] Ability to Move Files
	- [x] Ability to update map
	- [ ] Ability to create and delete new folders/nodes
	- [ ] Ability to create new projects/boards/maps
	- [ ] Ability to change names of particular directories

---
- [X] Finish Options #important

---
- [ ] Security #important
	- [X] Privacy Policy
	- [X] Terms of Use
	- [ ] Add disable cache storing

---

- [ ] Website #lessimportant
	- [ ] Add pictures and headers to the landing page.
	- [ ] Fix Grammar

[[Datoteka.]]
