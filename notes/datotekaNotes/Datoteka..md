# Datoteka
## Overview
As the development in technology has increased, the necessity for having a computer is growing as rapidly as the insufficiency to keep them clean of digital clutter. Digital clutter, a combination of forgotten photos, files, documents, among other things, are silent productivity-killers, and can slowly accumulate over time.

If you have a hard time organizing your computer files, photos, and links, you are not alone. Finding files or documents steals minutes, hours, or even days.

Current solutions, such as Tagspaces or Tables, use different methods to help you organize, but may be complicated for the everyday user. To solve this, I developed Datoteka. Datoteka takes Finder or File Explorer and simplifies how it visualizes, sorts, and names your files. Datoteka uses a node map, which resembles the branches of a tree. The node map allows users to see the size, name, and type of files they have in a particular folder with ease and clarity. Datoteka focuses on the essentials rather than on customizing the information, so that the user can easily navigate throughout the app. 

Datoteka has been designed for three different audiences: the everyday consumer, developers, and photographers. For everyday consumers, it focuses on simplicity by providing a simple user-interface (UI) and compact design. It also provides in-app menus and quick-start guides to learning the different shortcuts. 

For developers, Datoteka has been mapped with keyboard shortcuts to all the app’s actions. This feature gives developers a way to sort more efficiently, allowing for gauged flexibility and control. Datoteka also comes equipped with a search bar that helps users narrow their search by allowing them to customize the search and its results. 

Photographers can capitalize on Datoteka's automatic sorting system, which organizes items by file extension, size, and/or type, whichever the user requests. The app uses image meta-data (such as place, time of capture, and size) and image recognition to sort and name photos. 



## Process
Ever since the beginning of the project, Datoteka was orchestrated to follow the design thinking process, and since last summer, Datoteka has gone through one brainstorm, two research, one design, and one prototype phase. 

The brainstorm phase focused on figuring out and determining the actual problem and finding solutions already out there. We brainstormed projects such as, Using Image Recognition to Categorize Stressed and Non - Stressed Plants, Faster File Transfer, Rethinking Customer Relationship Management (CRM) Software, and File Organization. Additionally, I interviewed my friends and family, in a process called project stories, to try and locate additional problems. In the end, I decided to work on file organization as it was the most scalable project. 

Right after brainstorming I quickly moved on to researching. This stage was focused on collecting information on current file organizers, such as Tabbles, Tagspaces, Pathfinder, and Astro File Manager. These apps focused on providing more tools to people who need flexibility when categorizing files, and typically were more complicated than default file organization systems. To minimize this, the file organizer has to be easy to navigate — yet still have the ability to customize how you organize items. This allowed me to narrow down some of the core features of the app. 
First, Datoteka had to be able to search quickly for files and allow attributes to be added to the search, such as adding a particular file extension. Second, the app needed to be able to remember past searches so that users didn’t have to enter a new search query every time. Third, and finally, Datoteka would need a way to quickly sort and categorize photos via image recognition.

With this I moved on to the design phase. Since the beginning of the project I knew that I wanted to display files in a node map. Similar to how neurons look, these nodes would represent folders (or directories) within your computer. During the design phase I created three different sketches of what the product might look like. 

The first design focused on getting core concepts down. This design had many issues, such as the complicated navigation and UI, or the abnormal menu system and search bar location. Interestingly enough, one of my mentors said that it needed to be simplified. So I went back and came up with the second design.

The second design was much more concise and precise, with one screen showing everything at once. This design showed several important issues or problems for users, such as keeping everything simple and mapping every action — even opening windows — to a keybind or shortcut. The particular design also needed a navigation rework to remove unnecessary user interactions. The key takeaway from this design was the more actions a user takes, the worse the product.

The third design was a complete overhaul — reduced to the core features and ideas. After making this design, I was given the approval of my mentors and started to develop the app.



## Development
So far I have built the preliminary parts of the app, such as the map, shortcuts, data gathering system, and menu systems. Datoteka is built on the JavaScript framework, Electron. Electron allows me to build cross-platform desktop apps with JavaScript, HTML, and CSS — all of which are programming languages used to build web apps. Additionally, Datoteka takes advantage of JQuery, D3, AnimeJS, Material Components for the Web (MCW), and other pre-installed Node modules. The project is open sourced and can be accessed through my gitlab page @Povellesto. 

I would like to thank my mentors who helped me throughout this process. Muhammed Othman, Rene Dias, Enoch Wu, John Feland, and Jen Selby. Additionally, Datoteka is an internship project in collaboration with Oroboro Studios, and you can find their work at oroboro.studio. I plan to stick with using the design process for future QUEST projects, and will continue developing Datoteka in my free time. Thanks for listening!


[[Privacy Policy]]
[[🌎Research]]
[[Todo List]]

