<h1> Privacy Policy </h1>

<p>Datoteka is an open source application, which means that anyone can access and manipulate the application as they so desire. However, Datoteka is committed to protecting the privacy of its users. What our team controls are any packaged apps located on <a href = "https://povellesto.gitlab.io/datoteka./">our website</a>, and any code uploaded to our <a href = "https://gitlab.com/Povellesto/datoteka.">Gitlab page</a>. This privacy policy only attributes to packaged versions of Datoteka on our website, and our code located in our Gitlab page.</p>

<h4>User Information</h4>
<p>Datoteka does not collect any information about its users. This includes, but is not limited to, IP addresses, locations, emails, passwords, files, images, etc. However, we reserve the right to collect user information. When doing so users will be prompted where they can choose to decline.</p>

<h4>Caches/Indexes</h4>
<p>Datoteka relies on storing local caches so that it knows what is on your machine, these caches are kept local to increase optimal preformance and reduce CPU usage. If the user is not comfortable with local cache/index, they can disable this feature in the privacy settings of the app. (not applicable to beta, or prebeta versions)</p>

<h4>Auditing this privacy policy</h4>
<p>This privacy policy will be amended, edited, and audited throughout development stages and in the future. If you have any questions or concerns feel free to reach out via the contact form below.</p>

<p><i>This privacy policy is affective as of Monday, September 28, 2020.</i></p>
