---
layout: default
title: Home
---
<header>
<h1>This is Datoteka. A redeveloped, completely free, <br /> <a href="https://gitlab.com/Povellesto/datoteka">open-scourced</a> file-organizer.</h1>

<h2>Current Stage | <i> Development</i></h2>
<h3>Minimal Viable Build (Product) | <i> project paused </i></h3>

<div class="box alt">
<div class="row uniform">
<div class="12u$"><span class="image fit"><img src="images/datotekaBeta.jpg" alt="" /></span></div>
</div>
</div>

<p>People have a hard time organizing their files, photos, and links. Additionally, most people don't develop a simple and easy sorting system that they can follow regularly. In return, they have trouble locating files or important documents, which can take up to five to ten minutes - if not hours to find. Current solutions to this problem, such as Tagspaces or Tabbles, take advantage of different methods to allow the user to customize where and what is being organized — but makes the process harder to navigate. To solve this, Datoteka takes the pre-existing file organizing system and simplifies how preinstalled file organizers visualize, sort, and name large amounts of data. </p>

<p>Datoteka has been designed to tend to three different audiences at once, the normal consumer, developers, and photographers.</p>

<p>For normal consumers, Datoteka focuses on simplicity by providing a simple UI and compact design. It also provides in-app menus and quick start guides to learning the different shortcuts and a simple "normal" search bar. For developers, Datoteka has been mapped with a keyboard shortcut to every single action that can take place within the app. This feature allows developers a faster and more efficient pace than normal, allowing for gauged flexibility and control. Additionally, Datoteka also comes equipped with a search bar that helps users narrow down their search by allowing users to customize search results. Photographers, videographers, editors, and more, can capitalize on Datoteka's complex automatic sorting system, which sorts items by particular file extensions, size, or type. Additionally, it uses image meta-data and image recognition to sort and name photos automatically.</p>

<p>Datoteka is currently built on the Electron framework and takes advantage of JQuery, minisearch, fs, D3, AnimeJS, MCW (Material Components for the Web), and other preinstalled node modules.</p>

<p><i>Datoteka is an internship project in collaboration with <a href="https://oroboro.studio/">Oroboro Studios</a>. Special thanks to Muhammed Othman, Rene Dias, Enoch Wu, John Feland, Jen Selby, and many other close friends and family.</i></p>

{% include tiles.html %}
